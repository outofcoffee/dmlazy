# dmlazy: docker-machine initialisation for lazy people.

## What does it do?

Ensures docker-machine is in a running state (runs ``docker machine start <machine name>``, if machine is in *Stopped* or *Saved* state), then sets up environment variables (runs ``docker-machine env <machine name>``).

## Usage

Open a terminal window and type:

    dmlazy

Congratulations - your machine should be running and your environment variables set up.

## Setup

### Prerequisites

Ensure you have ``docker-machine`` version 0.4+ installed, and that you have created a machine.

The machine name is set in the script. You can change it by amending the ``DOCKER_MACHINE_NAME`` variable.

### Step by step

Step 1: Set up an alias for your shell:

For bash:

    ./dmlazy bash

For zsh:

    ./dmlazy zsh

Step 2: Restart your shell.

Step 3: Run the command: ``dmlazy``